#!/bin/bash
# Andrew Elster (andrew@astonishdesigns.com) 05/20/2011
# TODO: Check for parameters and output instructions if none found
# TODO: Backup ~/.bashrc, ~/.gitconfig before appending to those files.

# Usage
# Copy into quickstart/etc
# Pass in user name and email for git config 
# (i.e. install-supergit.sh "first last" name@domain.com )


########################################################################
## Git-Flow
echo "Installing git-flow..."
wget --no-check-certificate -q -O - https://github.com/nvie/gitflow/raw/develop/contrib/gitflow-installer.sh | sudo sh

########################################################################
## Git Bash Completion
echo "Installing git bash-completion..."
sudo apt-get install bash-completion

########################################################################
## Git-Flow Bash Completion
echo "Installing git-flow bash-completion..."
git clone git://github.com/bobthecow/git-flow-completion.git
ln -s ./git-flow-completion/git-flow-completion.bash ~/.git-flow-completion.sh

cat >> ~/.bashrc <<END

##
# Bash completion scripts
##
source ~/.git-flow-completion.sh
END

########################################################################
## Setup git-aware profile
echo "Configuring bashrc for a git-aware shell (nifty colors etc.)..."
cat >> ~/.bashrc <<END

##
# Shell colors
##
BLACK="\[\e[0;30m\]"  BOLD_BLACK="\[\e[1;30m\]"  UNDER_BLACK="\[\e[4;30m\]"
RED="\[\e[0;31m\]"    BOLD_RED="\[\e[1;31m\]"    UNDER_RED="\[\e[4;31m\]"
GREEN="\[\e[0;32m\]"  BOLD_GREEN="\[\e[1;32m\]"  UNDER_GREEN="\[\e[4;32m\]"
YELLOW="\[\e[0;33m\]" BOLD_YELLOW="\[\e[1;33m\]" UNDER_YELLOW="\[\e[4;33m\]"
BLUE="\[\e[0;34m\]"   BOLD_BLUE="\[\e[1;34m\]"   UNDER_BLUE="\[\e[4;34m\]"
PURPLE="\[\e[0;35m\]" BOLD_PURPLE="\[\e[1;35m\]" UNDER_PURPLE="\[\e[4;35m\]"
CYAN="\[\e[0;36m\]"   BOLD_CYAN="\[\e[1;36m\]"   UNDER_CYAN="\[\e[4;36m\]"
WHITE="\[\e[0;37m\]"  BOLD_WHITE="\[\e[1;37m\]"  UNDER_WHITE="\[\e[4;37m\]"
NO_COLOR="\[\e[0m\]"


##
# Git shell prompt
##
function parse_git_branch {
  git branch --no-color 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/'
}
function parse_git_dirty {
  [[ \$(git status 2> /dev/null | tail -n1) != "nothing to commit (working directory clean)" ]] && echo "*"
}
# export PS1="\${PURPLE}\h\${WHITE}[\${YELLOW}\w\${WHITE}]\${WHITE}[\${GREEN}\\\$(parse_git_branch)${RED}\\\$(parse_git_dirty)\${WHITE}]\${NO_COLOR} "
export PS1="\${PURPLE}\h\${WHITE}[\${YELLOW}\w\${WHITE}]\\\$(__git_ps1 '\${WHITE}[\${GREEN}%s\${RED}'\\\$(parse_git_dirty)'\${WHITE}]')\${WHITE}\${NO_COLOR} "
export PS2=" > "
export PS4=" + "
END

########################################################################
## Setup git configuration
echo "Creating super duper git config..."
cat > ~/.gitconfig <<END
[user]
  name = $1
  email = $2
[alias]
  br = branch
  ci = commit
  cia = commit -a
  co = checkout
  df = diff
  lg = log -p
  st = status
  sth = stash
  up = pull origin
  who = shortlog -s --
  spull = !git-svn rebase
  spush = !git-svn dcommit
  ddiff = !git-diff --no-prefix
  # `git last 2 user` displays last two days of commits on all local and remote branches for user
  # user can be left off to display your own
  last = "!fn() { S=1; A=\`git config --get user.name\`; if [ -n \"\$1\" ]; then S=\"\$1\"; shift; fi; if [ -n \"\$1\" ]; then if [ \"\$1\" != \"me\" ]; then A=\"\$1\"; fi; shift; fi; git log -i --since=\"\$S days ago\" --author=\"\$A\" --branches --remotes --format='%C(yellow)%t%Creset %s (%an)' \"\$@\"; }; fn"
[color]
  ui = auto
  branch = auto
  diff = auto
  status = auto
[push]
  default = tracking
[remote "origin"]
  push = HEAD
[branch]
  autosetupmerge = true
[merge]
  tool = meld
[mergetool]
  prompt = false
  keepBackup = false
[mergetool "diffmerge"]
  cmd = /usr/bin/env diffmerge --merge --result="\$MERGED" "\$LOCAL" "\$BASE" "\$REMOTE"
  trustExitCode = false
[diff]
  tool = meld
[difftool]
  prompt = false
[difftool "diffmerge"]
  cmd = /usr/bin/env diffmerge "\$LOCAL" "\$REMOTE"
END

########################################################################
## Setup git configuration
echo "Setting up Drush command aliases and conveniences..."
cat >> ~/.bashrc <<END
alias drsp='cp sites/default/default.settings.php sites/default/settings.php'
alias drcc='drush cache-clear all'
alias drdb='drush updb && drush cc all'
alias drdu='drush sql-dump --ordered-dump --result-file=dump.sql'
alias dren='drush pm-enable'
alias drdis='drush pm-disable'
alias drf='drush features'
alias drfd='drush features-diff'
alias drfu='drush -y features-update'
alias drfr='drush -y features-revert'
alias drfra='drush -y features-revert all'
alias dr='drush'

# Completion. For personal use, just copy all the code below to
# the end of .bashrc; for system-wide use, copy to a file like
# /etc/bash_completion.d/drush_custom instead.

_drupal_root() {
  # Go up until we find index.php
  current_dir=\`pwd\`;
  while [ \${current_dir} != "/" -a -d "\${current_dir}" -a \
          ! -f "\${current_dir}/index.php" ] ; 
  do
    current_dir=\$(dirname "\${current_dir}") ;
  done
  if [ "\$current_dir" == "/" ] ; then
    exit 1 ;
  else
    echo "\$current_dir" ;
  fi 
}

_drupal_modules_in_dir()
{
  COMPREPLY=( \$( compgen -W '\$( command find \$1 -regex ".*\.module" -exec basename {} .module \; 2> /dev/null )' -- \$cur  ) )
}

_drupal_modules()
{
  local cur
  COMPREPLY=()
  cur=\${COMP_WORDS[COMP_CWORD]}
  local drupal_root=\`_drupal_root\` && \
  _drupal_modules_in_dir "\$drupal_root/sites \$drupal_root/profiles \$drupal_root/modules"
}

_drupal_features_in_dir()
{
  COMPREPLY=( \$( compgen -W '\$( command find \$1 -regex ".*\.features.inc" -exec basename {} .features.inc \; 2> /dev/null )' -- \$cur  ) )
}

_drupal_features()
{
  local cur
  COMPREPLY=()
  cur=\${COMP_WORDS[COMP_CWORD]}
  local drupal_root=\`_drupal_root\` && \
  _drupal_features_in_dir "\$drupal_root/sites \$drupal_root/profiles \$drupal_root/modules"
}

cdd()
{
  local drupal_root=\`_drupal_root\` && \
  if [ "\$1" == "" ] ; then
    cd "\$drupal_root";
  else
    cd \`find \$drupal_root -regex ".*/\$1\.module" -exec dirname {} \;\`
  fi
}

complete -F _drupal_modules dren
complete -F _drupal_modules drdis
complete -F _drupal_features drfr
complete -F _drupal_features drfu
complete -F _drupal_features drfd
complete -F _drupal_modules cdd
END



cat <<EOF

******************************************************************************
This script has finished.  Enjoy your beefier git and drush setup!
******************************************************************************

Improvements installed:
 * Git flow - convenience functions for high level branch management
   - http://nvie.com/posts/a-successful-git-branching-model
   - http://jeffkreeftmeijer.com/2010/why-arent-you-using-git-flow
    
 * Git Bash Completion - Tab completion of git stuff
   
 * Git-Flow Bash Completion - Tab completion of git-flow commands and branches
   - https://github.com/bobthecow/git-flow-completion
   
 * Git aware shell (Thanks Work Habit)
   - When inside a repository, the shell will append the current branch to the
     end of the path, as well as a * if there are uncommitted changes.
     For example: username[/path/to/repository][master*]
 
 * Awesome git configuration (Thanks Work Habit) - command aliases and 
                                                  pretty colors
   - git st           git status
   - git ci           git commit
   - git cia          git commit -a
   - git br           git branch
   - git co           git checkout
   - git df           git diff
   - git lg           git log -p
   - git sth          git stash
   - git up           git pull origin
   - git who          git shortlog -s --
   - git spull        git !git-svn rebase
   - git spush        git !git-svn dcommit
   - git ddiff        git !git-diff --no-prefix
   - git last [user]  Shows last two days of commits on all local and remote 
                      branches for user (optional). If user is not specified,
                      then it shows your commits.
 
 * Drush aliases and completions (Thanks Nuvole)
   - cdd              jump to Drupal root directory from within the Drupal tree
   - cdd vie[TAB]     Autocomplete "cdd views" and jump to Views module 
                      directory
   - dr[fu,fr,fd] feature_c[TAB]
                      Autocomplete "feature_cool" and run drush [fu, fr, fd] 
                      feature_cool 
EOF
